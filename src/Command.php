<?php namespace Console2;
use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\RuntimeException;

/** 
* author: Tomasz Drzymalski
* purpose:  Sort JSON string with products by price ascending, 
*           and if price is the same sort alphabetically ascending.
*/
class Command extends SymfonyCommand
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function getSortedJSON(InputInterface $input, OutputInterface $output)
    {
        $output->write($this->sortJSON($input->getArgument('jsonstring')));
    }

    private function sortJSON($json)
    {
        try {
            $jsonarray = json_decode($json);

            if (!json_last_error() == JSON_ERROR_NONE){
                throw new RuntimeException('Given string is not valid JSON object');
            }

            $priceindex  = array_column($jsonarray, 'price');
            $titleindex  = array_column($jsonarray, 'title');
            $titleindex = array_map('strtolower', $titleindex);

            array_multisort($priceindex, SORT_ASC, SORT_NUMERIC, 
                            $titleindex, SORT_ASC, SORT_NATURAL,  
                            $jsonarray);

            return  json_encode($jsonarray);
        } catch (RuntimeException $e) {
            print($e->getMessage());
        }
    }
}