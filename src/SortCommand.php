<?php namespace Console2;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Console2\Command;

/** 
* author: Tomasz Drzymalski
*/

class SortCommand extends Command
{
    public function configure()
    {
        $this   ->setName('jsonsort')
                ->setDescription('Sort JSON string with products by price ascending. If price is the same sort alphabetically ascending.')
                ->setHelp('Pass JSON string to sort it')
                ->addArgument('jsonstring', InputArgument::REQUIRED, 'JSON you want to sort');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getSortedJSON($input, $output);
    }
}