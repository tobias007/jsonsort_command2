#!/usr/bin/env php
<?php
require_once __DIR__ . '/vendor/autoload.php';
use Symfony\Component\Console\Application;
use Console2\SortCommand;

/** 
* author: Tomasz Drzymalski
* purpose:  Sort JSON string with products by price ascending, 
*           and if price is the same sort alphabetically ascending.
*/

$app = new Application();
$app->add(new SortCommand());
$app->run();