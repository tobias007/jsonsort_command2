###Instructions:


Clone repository using:
```
	git clone https://tobias007@bitbucket.org/tobias007/jsonsort_command2.git
```
Move to project directory
```
	cd {...}/jsonsort_command2
```

Run command in CLI(e.g.Git Bash, Docker Quickstart Terminal):
```
	php console.php jsonsort  '{JSON string}'
```
for example:
```
	php console.php jsonsort '[{"title": "H&M T-Shirt White","price": 10.99,"inventory": 10},{"title": "Magento Enterprise License","price": 1999.99,"inventory": 9999},{"title": "iPad 4 Mini","price": 500.01,"inventory": 2},{"title": "iPad Pro","price": 990.20,"inventory": 2},{"title": "Garmin Fenix 5","price": 789.67,"inventory": 34},{"title": "Garmin Fenix 3 HR Sapphire Performer Bundle","price": 789.67,"inventory": 12}]'
```

If you want to run command on Windows CMD you need to escape special characters(e.g quotation marks)
```
php console2.php jsonsort  "[{\"title\": \"H^&M T-Shirt White\",\"price\": 10.99,\"inventory\": 10},{\"title\": \"Magento Enterprise License\",\"price\": 1999.99,\"inventory\": 9999},{\"title\": \"iPad 4 Mini\",\"price\": 500.01,\"inventory\": 2},{\"title\": \"iPad Pro\",\"price\": 990.20,\"inventory\": 2},{\"title\": \"Garmin Fenix 5\",\"price\": 789.67,\"inventory\": 34},{\"title\": \"Garmin Fenix 3 HR Sapphire Performer Bundle\",\"price\": 789.67,\"inventory\": 12}]"
```